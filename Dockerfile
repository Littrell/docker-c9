FROM node:10.10

RUN set -x \
    && mkdir /workspace \
    && apt-get update \
    && apt-get -y install git tmux \
    && git clone https://github.com/c9/core.git /c9sdk \
    && cd /c9sdk \
    && scripts/install-sdk.sh

WORKDIR /c9sdk

EXPOSE 8080

ENTRYPOINT node server.js -p 8080 -a : -w /workspace --listen 0.0.0.0 --collab
