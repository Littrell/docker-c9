#!/bin/bash

# Set the directory we're going to run Codebox in
workingdir=$(pwd)

# Move to Dockerfile directory
cd $(dirname "$0")

# Build image
docker build -t cnine:latest .

# Build container
docker run --rm -it -p 8080:8080 -v $workingdir:/workspace cnine:latest

cd $workingdir
